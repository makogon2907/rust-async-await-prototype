use std::{collections::HashMap};

use lib::message::Content;

#[derive(Default, Debug)]
pub struct PingMessage {}

impl Content for PingMessage {}

#[derive(Default, Debug)]
pub struct SyncMessage {
    pub key: String,
    pub value: String,
}

impl Content for SyncMessage {}

#[derive(Default, Debug)]
pub struct SyncOk {
    pub key: String,
}
impl Content for SyncOk {}

#[derive(Default, Debug)]
pub struct GetKeysRequest {}

impl Content for GetKeysRequest {}

#[derive(Default, Debug)]
pub struct GetKeysResponse {
    pub data: HashMap<String, String>,
}

impl Content for GetKeysResponse {}

#[derive(Default, Debug)]
pub struct InsertKeyRequest {
    pub key: String,
    pub value: String,
}

impl Content for InsertKeyRequest {}
