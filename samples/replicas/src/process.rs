use std::{cell::RefCell, collections::HashMap, rc::Rc};

use futures::future::select_all;
use futures::StreamExt;
use futures::{future::join_all, stream::FuturesUnordered};
use lib::{context::DSContext, event::EventResult};

use crate::messages::{GetKeysRequest, GetKeysResponse, InsertKeyRequest, SyncMessage, SyncOk};

#[derive(Clone)]
struct Context {
    base: DSContext,
    storage: Rc<RefCell<HashMap<String, String>>>,
}

async fn listen_get_requests(ctx: Context) {
    loop {
        ctx.base.wait_for_user_message::<GetKeysRequest>().await.unwrap_ok();
        ctx.base.send_message_to_user(GetKeysResponse {
            data: ctx.storage.borrow().clone(),
        })
    }
}

async fn listen_insert_requests(ctx: Context) {
    loop {
        let req = ctx.base.wait_for_user_message::<InsertKeyRequest>().await.unwrap_ok().data;

        ctx.storage.borrow_mut().insert(req.key.clone(), req.value);

        ctx.base.add_background(sync_data(ctx.clone(), req.key));
    }
}

async fn listen_sync_requests(ctx: Context, from: String) {
    loop {
        let req = ctx.base.wait_for_message_unlimited::<SyncMessage>(from.clone()).await;
        let data = req.unwrap_ok().data;

        ctx.storage.borrow_mut().insert(data.key.clone(), data.value);
        ctx.base.send_message(from.clone(), SyncOk { key: data.key });
    }
}

async fn sync_data(ctx: Context, key: String) {
    let value = ctx.storage.borrow().get(&key).unwrap().clone();

    let mut futures = FuturesUnordered::new();
    let mut timeout = 200;

    for node in ctx.base.nodes.iter() {
        ctx.base.send_message(
            node.clone(),
            SyncMessage {
                key: key.clone(),
                value: value.clone(),
            },
        );
        futures.push(ctx.base.wait_for_message::<SyncOk>(timeout, node.clone()));
    }

    while let Some(answer) = futures.next().await {
        match answer {
            EventResult::Timeout(res) => {
                timeout *= 2;
                futures.push(ctx.base.wait_for_message(timeout, res.from))
            }
            EventResult::Ok(_res) => {}
        };
        if timeout >= 1000 {
            break;
        }
    }

    println!("node: {} key {} synced", ctx.base.node_name, key);
}

async fn sync_all(ctx: Context) {
    loop {
        ctx.base.wait_for(600).await;

        println!("SYNC ALL BEGIN FROM {}", ctx.base.node_name);

        let tm = ctx.base.get_time();

        let mut keys = Vec::new();
        for (key, _) in ctx.storage.borrow().iter() {
            keys.push(key.clone());
        }

        for key in keys {
            sync_data(ctx.clone(), key).await;
        }

        println!("SYNC TAKES: {}", ctx.base.get_time() - tm);
    }
}

pub async fn start_storage_node(base_ctx: DSContext) {
    println!("Hello, I am {} and I am launched", base_ctx.node_name);

    let ctx = Context {
        base: base_ctx.clone(),
        storage: Rc::new(RefCell::new(HashMap::new())),
    };

    base_ctx.add_background(listen_get_requests(ctx.clone()));
    base_ctx.add_background(listen_insert_requests(ctx.clone()));

    for node in base_ctx.nodes.iter() {
        base_ctx.add_background(listen_sync_requests(ctx.clone(), node.clone()));
    }

    base_ctx.add_background(sync_all(ctx.clone()));
}
