use std::collections::HashMap;

use futures::{stream::FuturesUnordered, StreamExt};

use lib::context::{self, DSContext};
use lib::event::EventResult;

use lib::world::World;
use messages::{GetKeysRequest, GetKeysResponse, InsertKeyRequest};

mod messages;
mod process;

use crate::process::start_storage_node;

const GET_KEYS: &str = "get_keys";
const INSERT_KEY: &str = "insert_key";
const KEY_FIELD: &str = "key";
const VALUE_FIELD: &str = "value";

async fn ask_nodes_for_keys(ctx: &DSContext, _world: &World) -> HashMap<String, HashMap<String, String>> {
    for node in ctx.nodes.iter() {
        ctx.send_message(node.clone(), GetKeysRequest {})
    }

    let mut futures = FuturesUnordered::new();
    for node in ctx.nodes.iter() {
        futures.push(ctx.wait_for_message::<GetKeysResponse>(1, node.clone()))
    }

    let mut answer = HashMap::new();

    while let Some(result) = futures.next().await {
        match result {
            EventResult::Ok(ok) => {
                answer.insert(ok.from, ok.data.data);
            }
            EventResult::Timeout(_timeout) => unreachable!(),
        }
    }

    answer
}

fn add_key_for_node(ctx: &DSContext, node: String, key: String, value: String) {
    ctx.send_message(node, InsertKeyRequest { key, value })
}

async fn world_settings(ctx: DSContext, world: World, nodes: Vec<String>) {
    ctx.wait_for(10).await;

    for node in nodes.iter() {
        add_key_for_node(&ctx, node.clone(), node.clone(), "default_value".to_string())
    }

    ctx.wait_for(10).await;

    ask_nodes_for_keys(&ctx, &world).await;

    ctx.wait_for(300).await;

    ask_nodes_for_keys(&ctx, &world).await;

    ctx.wait_for(2000).await;

    ask_nodes_for_keys(&ctx, &world).await;

    world.stop();
}

fn main() {
    let (world, executor) = World::new();

    let nodes = vec!["node1".to_string(), "node2".to_string(), "node3".to_string()];

    world.set_delay_for_node("node1".to_string(), 20);
    world.set_delay_for_node("node2".to_string(), 50);
    world.set_delay_for_node("node3".to_string(), 70);

    for node in nodes.iter() {
        world.spawn(start_storage_node(DSContext::new(node.clone(), nodes.clone(), world.clone())));
    }

    let user_context = DSContext::new(context::USER_LOGIN.to_string(), nodes.clone(), world.clone());

    world.spawn(world_settings(user_context, world.clone(), nodes.clone()));

    world.run(&executor);
}
