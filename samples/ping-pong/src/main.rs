

use lib::context::DSContext;

use lib::world::World;

mod process;
use crate::process::start_ping_node;

fn main() {
    let (world, executor) = World::new();

    let nodes = vec![
        "node1".to_string(),
        "node2".to_string(),
        "node3".to_string(),
    ];

    world.set_delay_for_node("node1".to_string(), 20);
    world.set_delay_for_node("node2".to_string(), 40);
    world.set_delay_for_node("node3".to_string(), 60);

    for node in nodes.iter() {
        world.spawn(start_ping_node(DSContext::new(
            node.clone(),
            nodes.clone(),
            world.clone(),
        )));
    }

    world.run(&executor);
}
