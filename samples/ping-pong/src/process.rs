

use futures::{stream::FuturesUnordered, StreamExt};
use lib::{
    context::DSContext,
    event::EventResult,
    message::{Content},
};

#[derive(Default, Debug)]
struct PingMessage {
    data: String,
}

impl Content for PingMessage {}

pub async fn start_ping_node(ctx: DSContext) {
    println!("Hello, I am {} launched!", ctx.node_name);

    for node in ctx.nodes.iter() {
        ctx.send_message(
            node.clone(),
            PingMessage {
                data: format!("hello from {}", ctx.node_name),
            },
        )
    }

    let mut futures = FuturesUnordered::new();
    for node in ctx.nodes.iter() {
        futures.push(ctx.wait_for_message::<PingMessage>(100, node.clone()));
    }

    while let Some(result) = futures.next().await {
        match result {
            EventResult::Timeout(timeout) => {
                println!(
                    "node {}: message from node {} timeouted :(",
                    ctx.node_name, timeout.from
                );
            }
            EventResult::Ok(response) => {
                println!(
                    "node {}: from node {} got data {:?} ",
                    ctx.node_name, response.from, response.data
                );
            }
        }
    }
}
