use std::{cell::RefCell, cmp::Ordering, sync::Arc};

use crate::event::*;

pub struct Timer {
    pub time: usize,
    pub state: Arc<RefCell<dyn State>>,
}

impl Timer {
    pub fn new(time: usize, state: Arc<RefCell<dyn State>>) -> Self {
        Self { time, state }
    }
}

impl PartialEq for Timer {
    fn eq(&self, other: &Self) -> bool {
        self.time == other.time
    }
}

impl Eq for Timer {}

impl Ord for Timer {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for Timer {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.time.cmp(&other.time).reverse())
    }
}
