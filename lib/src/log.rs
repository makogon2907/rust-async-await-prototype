use colored::Colorize;



use crate::{
    message::{Content, MessageMetaInfo},
    world_inner::WorldInner,
};

pub fn log_timeout<T: Content>(inner: &WorldInner, msg_meta: &MessageMetaInfo) {
    println!(
        "{}",
        format!(
            "{} : {} x--- {}: {} timeouted",
            inner.get_current_time(),
            msg_meta.to,
            msg_meta.from,
            std::any::type_name::<T>()
        )
        .red()
    );
}

pub fn log_receive<T: Content>(inner: &WorldInner, msg_meta: &MessageMetaInfo, content: &T) {
    println!(
        "{}",
        format!(
            "{} : {} <--- {}: {:?}",
            inner.get_current_time(),
            msg_meta.to,
            msg_meta.from,
            content
        )
        .green()
    );
}

pub fn log_wait<T: Content>(inner: &WorldInner, msg_meta: &MessageMetaInfo) {
    println!(
        "{}",
        format!(
            "{} : {} will have to wait until {} send something like {}",
            inner.get_current_time(),
            msg_meta.to,
            msg_meta.from,
            std::any::type_name::<T>(),
        )
        .truecolor(250, 250, 250)
    );
}

pub fn log_send<T: Content>(inner: &WorldInner, msg_meta: &MessageMetaInfo, content: &T) {
    println!(
        "{}",
        format!(
            "{} : {} ---> {}: {:?}",
            inner.get_current_time(),
            msg_meta.from,
            msg_meta.to,
            content
        )
        .green()
    );
}

pub fn log_fail_send<T: Content>(inner: &WorldInner, msg_meta: &MessageMetaInfo, content: &T) {
    println!(
        "{}",
        format!(
            "{} : {} ---x {}: {:?}",
            inner.get_current_time(),
            msg_meta.from,
            msg_meta.to,
            content
        )
        .red()
    );
}
