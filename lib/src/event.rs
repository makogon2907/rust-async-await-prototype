


use std::{cell::RefCell, future::Future, sync::Arc, task::Context};

use std::{
    pin::Pin,
    task::{Poll, Waker},
};

use crate::message::{Content};

#[derive(Clone, Default)]
pub struct ResultTimeout {
    pub from: String,
}

#[derive(Clone, Default)]
pub struct ResultResponse<T: Content> {
    pub from: String,
    pub data: T,
}

#[derive(Clone)]
pub enum EventResult<T: Content> {
    Timeout(ResultTimeout),
    Ok(ResultResponse<T>),
}

impl<T: Content> Default for EventResult<T> {
    fn default() -> Self {
        Self::Timeout(ResultTimeout::default())
    }
}

impl<T: Content> EventResult<T> {
    pub fn unwrap_ok(self) -> ResultResponse<T> {
        match self {
            Self::Ok(v) => v,
            Self::Timeout(_) => panic!("unwrap timeout result!"),
        }
    }
}

pub struct EventFuture<T: Content> {
    pub state: Arc<RefCell<SharedState<T>>>,
}

impl<T: Content> Future for EventFuture<T> {
    type Output = EventResult<T>;
    fn poll(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<Self::Output> {
        // println!("Polling EventFuture...{}", self.state.borrow().completed);
        let mut state = self.state.as_ref().borrow_mut();

        if !state.completed {
            state.waker = Some(_cx.waker().clone());
            return Poll::Pending;
        }

        state.delivered = true;

        let mut filler = EventResult::default();
        std::mem::swap(&mut filler, &mut state.shared_content);

        return Poll::Ready(filler);
    }
}

#[derive(Clone, Default)]
pub struct SharedState<T: Content> {
    /// Whether or not the sleep time has elapsed
    pub completed: bool,
    pub delivered: bool,
    /// The waker for the task that `TimerFuture` is running on.
    /// The thread can use this after setting `completed = true` to tell
    /// `TimerFuture`'s task to wake up, see that `completed = true`, and
    /// move forward.
    pub waker: Option<Waker>,

    pub shared_content: EventResult<T>,
}

impl<T: Content> State for SharedState<T> {
    fn set_completed(&mut self) {
        if self.completed {
            return;
        }
        self.completed = true;
        if let Some(waker) = self.waker.take() {
            waker.wake()
        }
    }
}

impl<T: Content> SharedState<T> {
    pub fn set_ok_content(&mut self, content: ResultResponse<T>) {
        self.shared_content = EventResult::Ok(content);
        self.set_completed();
    }

    pub fn set_timeouted(&mut self, content: ResultTimeout) {
        self.shared_content = EventResult::Timeout(content);
        self.set_completed();
    }
}

pub trait State {
    fn set_completed(&mut self);
}
