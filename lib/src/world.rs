use std::{cell::RefCell, future::Future, sync::Arc};

use crate::task::Task;
use crate::world_inner;
use crate::{
    event::ResultTimeout,
    message::{Content, EmptyContent},
    world_inner::InnerMessage,
};
use crate::{
    event::{EventFuture, EventResult, SharedState},
    world_inner::WorldInner,
};

use crate::executor::Executor;

#[derive(Clone)]
pub struct World {
    inner: Arc<WorldInner>,
}

impl World {
    pub fn new() -> (Self, Executor) {
        let (inner, executor) = WorldInner::new();
        (Self { inner: Arc::new(inner) }, executor)
    }

    pub fn spawn(&self, future: impl Future<Output = ()> + 'static) {
        let task = Arc::new(Task::new(future, self.inner.task_sender.clone()));

        self.inner.task_sender.send(task).expect("too many tasks queued");
    }

    pub fn run(&self, executor: &Executor) {
        loop {
            executor.process_tasks();
            if *self.inner.is_stopped.borrow() || !self.inner.update_timers() {
                return;
            }
        }
    }

    pub fn stop(&self) {
        *self.inner.is_stopped.borrow_mut() = true;
    }

    pub fn send_message<T: Content>(&self, msg: InnerMessage<T>) {
        let delay = self.inner.get_delay_for_msg(&msg);

        self.spawn(world_inner::inner_send(self.inner.clone(), delay, msg));
    }

    pub fn wait_for_message_from<T: Content>(&self, timeout: isize, asker: String, from: String) -> EventFuture<T> {
        let state = Arc::new(RefCell::new(SharedState {
            shared_content: EventResult::<T>::Timeout(ResultTimeout { from: from.clone() }),
            completed: false,
            delivered: false,
            waker: None,
        }));

        self.spawn(world_inner::inner_wait::<T>(
            self.inner.clone(),
            timeout,
            asker,
            from,
            state.clone(),
        ));

        EventFuture { state }
    }

    pub fn wait_for(&self, delay: usize) -> EventFuture<EmptyContent> {
        self.inner.wait_for(delay)
    }

    pub fn set_delay_for_node(&self, node: String, delay: usize) {
        self.inner
            .node_delays
            .borrow_mut()
            .entry(node)
            .and_modify(|a| *a = delay)
            .or_insert(delay);
    }

    pub fn get_current_time(&self) -> usize {
        self.inner.get_current_time()
    }
}
