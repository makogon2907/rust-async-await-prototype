use futures::stream::FuturesUnordered;
use rand::Rng;

use std::{
    any::{Any, TypeId},
    cell::RefCell,
    collections::{BinaryHeap, HashMap, VecDeque},
    sync::{
        mpsc::{sync_channel, SyncSender},
        Arc,
    },
    vec,
};

use futures::StreamExt;

use crate::{
    context::USER_LOGIN,
    event::{EventFuture, EventResult, ResultResponse, ResultTimeout, SharedState, State},
    executor::Executor,
    log::{log_fail_send, log_receive, log_send, log_timeout, log_wait},
    message::{Content, EmptyContent, MessageMetaInfo},
    task::Task,
    timer::Timer,
};

pub struct InnerMessage<T: Content> {
    pub content: T,
    pub from: String,
    pub to: String,
}

impl<T: Content> InnerMessage<T> {
    pub fn from_message(msg: T, from: String, to: String) -> Self {
        Self { content: msg, from, to }
    }

    fn get_meta(&self) -> MessageMetaInfo {
        MessageMetaInfo {
            from: self.from.clone(),
            to: self.to.clone(),
            msg_type: TypeId::of::<T>(),
        }
    }
}

pub struct WorldInner {
    pub is_stopped: RefCell<bool>,

    pub clock: RefCell<usize>,

    pub task_sender: SyncSender<Arc<Task>>,

    pub timers: RefCell<BinaryHeap<Timer>>,

    pub node_delays: RefCell<HashMap<String, usize>>,

    completed_mesages: RefCell<HashMap<MessageMetaInfo, VecDeque<Box<dyn Any>>>>,

    waiting_states: RefCell<HashMap<MessageMetaInfo, Arc<RefCell<dyn Any>>>>,
}

impl WorldInner {
    pub fn new() -> (Self, Executor) {
        const MAX_QUEUED_TASKS: usize = 10_000;
        let (task_sender, ready_queue) = sync_channel(MAX_QUEUED_TASKS);
        (
            Self {
                is_stopped: RefCell::new(false),

                clock: RefCell::new(0),
                task_sender,

                timers: RefCell::new(BinaryHeap::new()),

                completed_mesages: RefCell::new(HashMap::new()),
                waiting_states: RefCell::new(HashMap::new()),

                node_delays: RefCell::new(HashMap::new()),
            },
            Executor { ready_queue },
        )
    }

    pub fn update_timers(&self) -> bool {
        let mut timers = self.timers.borrow_mut();
        let top_timer = timers.pop();

        match top_timer {
            None => false,
            Some(timer) => {
                *self.clock.borrow_mut() = timer.time;

                timer.state.as_ref().borrow_mut().set_completed();

                while !timers.is_empty() && timers.peek().unwrap().time == timer.time {
                    timers.pop().unwrap().state.as_ref().borrow_mut().set_completed();
                }

                true
            }
        }
    }

    pub fn add_timer(&self, timer: Timer) {
        self.timers.borrow_mut().push(timer);
    }

    pub fn get_current_time(&self) -> usize {
        *self.clock.borrow()
    }

    pub fn wait_for(&self, delay: usize) -> EventFuture<EmptyContent> {
        let state = Arc::new(RefCell::new(SharedState {
            shared_content: EventResult::Timeout(ResultTimeout::default()),
            completed: false,
            delivered: false,
            waker: None,
        }));

        self.add_timer(Timer::new(self.get_current_time() + delay, state.clone()));

        EventFuture { state }
    }

    pub fn wait_for_timeout<T: Content>(&self, delay: usize) -> EventFuture<T> {
        let state = Arc::new(RefCell::new(SharedState::default()));

        self.add_timer(Timer::new(self.get_current_time() + delay, state.clone()));

        EventFuture { state }
    }

    pub fn get_delay_for_msg<T: Content>(&self, msg: &InnerMessage<T>) -> isize {
        let mut rng = rand::thread_rng();
        if msg.from == msg.to || msg.from.as_str() == USER_LOGIN || msg.to.as_str() == USER_LOGIN {
            return 0;
        }

        let raw_delay = self.node_delays.borrow()[&msg.from] as isize + self.node_delays.borrow()[&msg.to] as isize;

        return raw_delay + rng.gen_range(0, raw_delay / 4);
    }
}

pub async fn inner_wait<T: Content>(
    inner: Arc<WorldInner>,
    timeout: isize,
    asker: String,
    from: String,
    state: Arc<RefCell<SharedState<T>>>,
) {
    let mut states = inner.waiting_states.borrow_mut();
    let mut ready = inner.completed_mesages.borrow_mut();
    let msg_meta = MessageMetaInfo::new::<T>(from.clone(), asker.clone());

    if let Some(queue) = ready.get_mut(&msg_meta) {
        if let Some(last) = queue.pop_front() {
            let data = *last.downcast::<T>().unwrap();

            log_receive(inner.as_ref(), &msg_meta, &data);

            state.borrow_mut().set_ok_content(ResultResponse { from: msg_meta.from, data });

            return;
        }
    }

    if let Some(a) = states.get(&msg_meta) {
        let mut state = a.borrow_mut();

        if !state.downcast_mut::<SharedState<T>>().unwrap().completed {
            panic!("Somebody is already waiting at: {:?}: {}", msg_meta, std::any::type_name::<T>());
        }
    }

    log_wait::<T>(inner.as_ref(), &msg_meta);

    let tmp_state: Arc<RefCell<SharedState<T>>> = Arc::new(RefCell::new(SharedState::default()));

    states
        .entry(msg_meta.clone())
        .and_modify(|a| *a = tmp_state.clone())
        .or_insert(tmp_state.clone());

    drop(states);
    drop(ready);

    let mut futures = FuturesUnordered::new();

    futures.push(EventFuture { state: tmp_state });

    if timeout > 0 {
        futures.push(inner.wait_for_timeout::<T>(timeout as usize))
    }

    let fist_res = futures.next().await.unwrap();

    match fist_res {
        EventResult::Ok(response) => {
            log_receive(inner.as_ref(), &msg_meta, &response.data);

            state.borrow_mut().set_ok_content(response);
        }
        EventResult::Timeout(_) => {
            log_timeout::<T>(inner.as_ref(), &msg_meta);

            state.borrow_mut().set_timeouted(ResultTimeout {
                from: msg_meta.from.clone(),
            })
        }
    };

    inner.waiting_states.borrow_mut().remove(&msg_meta);

    // TODO in case of timeout drop state from states for key MsgMetaInfo
}

pub async fn inner_send<T: Content>(inner: Arc<WorldInner>, delay: isize, msg: InnerMessage<T>) {
    let msg_meta = msg.get_meta();

    if delay < 0 {
        log_fail_send(inner.as_ref(), &msg_meta, &msg.content);

        return;
    }

    log_send(inner.as_ref(), &msg_meta, &msg.content);

    inner.wait_for(delay as usize).await;

    let mut states = inner.waiting_states.borrow_mut();
    let mut ready = inner.completed_mesages.borrow_mut();

    if states.contains_key(&msg_meta) {
        states
            .get_mut(&msg_meta)
            .unwrap()
            .borrow_mut()
            .downcast_mut::<SharedState<T>>()
            .unwrap()
            .set_ok_content(ResultResponse {
                from: msg_meta.from,
                data: msg.content,
            });
    } else {
        match ready.get_mut(&msg_meta) {
            Some(queue) => queue.push_back(Box::new(msg.content)),
            None => {
                let new_queue: Vec<Box<dyn Any>> = vec![Box::new(msg.content)];
                ready.insert(msg_meta.clone(), VecDeque::from(new_queue));
            }
        };
    }
}
