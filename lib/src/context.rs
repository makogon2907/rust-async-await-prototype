use futures::Future;

use crate::{
    event::EventFuture,
    message::{Content, EmptyContent},
    world::World,
    world_inner::InnerMessage,
};

pub const USER_LOGIN: &str = "USER";

#[derive(Clone)]
pub struct DSContext {
    world: World,
    pub node_name: String,
    pub nodes: Vec<String>,
}

impl DSContext {
    pub fn new(node_name: String, nodes: Vec<String>, world: World) -> Self {
        Self { world, node_name, nodes }
    }

    pub fn send_message<T: Content>(&self, to: String, msg: T) {
        self.world.send_message(InnerMessage::from_message(msg, self.node_name.clone(), to))
    }

    pub fn wait_for(&self, delay: usize) -> EventFuture<EmptyContent> {
        self.world.wait_for(delay)
    }

    pub fn wait_for_message<T: Content>(&self, timeout: usize, from: String) -> EventFuture<T> {
        self.world
            .wait_for_message_from::<T>(timeout as isize, self.node_name.clone(), from)
    }

    pub fn wait_for_message_unlimited<T: Content>(&self, from: String) -> EventFuture<T> {
        self.world.wait_for_message_from::<T>(-1, self.node_name.clone(), from)
    }

    pub fn wait_for_user_message<T: Content>(&self) -> EventFuture<T> {
        self.world.wait_for_message_from(-1, self.node_name.clone(), USER_LOGIN.to_string())
    }

    pub fn send_message_to_user<T: Content>(&self, msg: T) {
        self.world
            .send_message(InnerMessage::from_message(msg, self.node_name.clone(), USER_LOGIN.to_string()))
    }

    pub fn get_time(&self) -> usize {
        self.world.get_current_time()
    }

    pub fn add_background(&self, future: impl Future<Output = ()> + 'static) {
        self.world.spawn(future)
    }
}
