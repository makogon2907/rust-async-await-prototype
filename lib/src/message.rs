use std::{
    any::{Any, TypeId},
    fmt::Debug,
};

pub trait Content: Any + Debug + Default {}

#[derive(Debug, Default)]
pub struct EmptyContent {}

impl Content for EmptyContent {}

#[derive(Clone, Default)]
pub struct Message<T: Clone + Default> {
    pub msg_type: String,
    pub content: T,
}

impl<T: Clone + Default> Message<T> {
    pub fn new(msg_type: String, content: T) -> Self {
        Self { msg_type, content }
    }
}

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
pub struct MessageMetaInfo {
    pub from: String,
    pub to: String,
    pub msg_type: TypeId,
}

impl MessageMetaInfo {
    pub fn new<T: Content>(from: String, to: String) -> Self {
        Self {
            from,
            to,
            msg_type: TypeId::of::<T>(),
        }
    }
}

// impl Message<HashMap<String, String>> {
//     fn new_dict(msg_type: &str, dict: HashMap<&str, &str>) -> Self {
//         Self {
//             msg_type: msg_type.to_string(),
//             content: dict
//                 .iter_mut()
//                 .map(|a| *a = (a.0.to_string(), a.1.to_string()))
//                 .collect(),
//         }
//     }
// }
